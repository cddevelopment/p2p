import java.io._

import Model.{Monitor, Node}
import Network.{Packet, PeerServer, PeerServer2}

object App{
    def main(args: Array[String]) {
        //testPeer(9562,9563)
        // testPeer(9563,9562)

        test()
    }

    def testPeer(port1: Int, port2: Int): Unit = {
        val ps = new PeerServer(port1, port2)
        new Thread(ps).start()
    }

    def testPeer2(port1: Int, node: Node): Unit = {
        val ps = new PeerServer2(port1, node)
        new Thread(ps).start()
    }

    def	testPacket(): Unit = {
        val filename = "testSerialization/t1.txt"
        val p: Packet = new Packet("INFO", "Je suis AS46546D")

        val fis = new FileInputStream(filename)
        val fos = new FileOutputStream(filename)

//        Packet.write(fos, p)
//        println(Packet.read(fis))
    }

    def testMonitor(): Unit = {
        new Thread(new Monitor).start()
    }

    def test(): Unit = {
        // On lance le moniteur
//        testMonitor()

        // On créé un premier pair
        testPeer2(9562, new Node("localhost"))
//        testPeer2(9563,9562)

    }
}