package Model

import java.net._

import Network.PeerServer2

object Chord {

    def main(args: Array[String]) {
        print("Que dois-je lancer ?")
        var cmd = ""
        val i = io.Source.stdin.getLines()
        if (i.hasNext) cmd = i.next()

        cmd match {
            case "m" =>
                // Démarrage monitor
                new Thread(new Monitor).start()
            case "1" =>
                println("Lancement 1")
//                val ip = "192.168.2.5"
                val ip = "p1"
                val node: Node = new Node(ip)
                // On lance le serveur
                val ps = new PeerServer2(Port.server, node)
                new Thread(ps).start()
                this.cmd(node)
            case "2" =>
//                val ip = "192.168.2.27"
                val ip = "p2"
                val node: Node = new Node(ip)
                // On lance le serveur
                val ps = new PeerServer2(Port.server2, node)
                new Thread(ps).start()
                this.cmd(node)
            case "c" =>
                println("Lancement client")
                //                val ip = "192.168.2.5"
                val ip = InetAddress.getLocalHost.getHostAddress
                Log.log("Notre ip : " + ip)
                val node: Node = new Node(ip)
                // On lance le serveur
                val ps = new PeerServer2(Port.server, node)
                new Thread(ps).start()
                this.cmd(node)
            case _ => println("Bad cmd")
        }

//        val ip = "localhost"
//
//
//
//        // Créer le noeud
//        val node: Node = new Node(ip)
//
//        // On lance le serveur
//        new PeerServer2(Port.server, node)
//
//        // On créer la finger table
//        node.install()
    }

    /**
     * Fait une action en fonction d'une commande
     *
     * Ex : Envoyer le message "Bonjour" à 192.168.1.27
     * > m 192.168.1.27 9991 "Bonjour"
     * > m localhost 9991 "Bonjour"
     * @param node
     */
    def cmd(node: Node): Unit ={
        while (true){
            print("Cmd > ")
            var input = ""
            val i = io.Source.stdin.getLines()
            if (i.hasNext) input = i.next()
            val splitedCmd = input split """ +(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)"""
            val cmd = splitedCmd.toList.map(x => x.replace("\"", ""))
            cmd match {
//              Send message
                case List("m", ip, port, message) =>
                    node.sendAndClose(ip, port.toInt, Method.message, message)
//              Envoie un message grâce à l'id
                case List("mi", id, port, message) =>
//                    Traduction id -> ip vers le monitor
                    node.idToIp(id.toInt) match {
                        case Some(ip) => node.sendAndClose(ip, port.toInt, Method.message, message)
                        case None => Log.log("Pas de correspondance trouvée")
                    }
                case List("table") => Log.log(node.fingerTable.toString)
                case _ => println("Bad cmd")
            }

        }
    }
}