package Model


object Port {
    final val server: Int = 9990
    final val server2: Int = 9991
    final val monitor: Int = 9999
}

object Method {
    final val hash: String = "HASH"
    final val hashResp: String = "HASH_RESPONSE"
    final val ipFromId: String = "IP_FROM_IP"
    final val ipFromIdResp: String = "IP_FROM_IP_RESP"
    final val connection: String = "CONNECTION"
    final val message: String = "MESSAGE"
    final val file: String = "FILE"
    final val predOf: String = "PRED_OF"
    final val repPredOf: String = "REP_PRED_OF"
    final val setPred: String = "SET_PRED"
    final val setSucc: String = "SET_SUCC"
    final val fingerTableUpdate: String = "FINGERTABLE_UPDATE"
}

object Other {
    final val maxPeers: Int = 100
//    final val serverIP: String = "192.168.2.5"
    final val serverIP: String = "localhost"
}