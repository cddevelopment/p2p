package Model

import scala.math._

class FingerTable(node: Node) extends Serializable {

    var nodes: List[Node] = List[Node]()

    var succ: Option[Node] = Some(node)
    var pred: Option[Node] = Some(node)

    var succID: Int = 0
    var predID: Int = 0
    if (node.id != 0){
        predID = node.id - 1
    }



    /**
     * Trouve le predéceseur qui est le plus proche du pair correspondant à l'id
     * @param id
     * @return
     */
    def pred(id: Int): Node = {
        if (this.nodes != null && this.nodes.nonEmpty)
            (this.nodes filter (n => n.id < id)) reduceLeft (_ maxId _)
        else

            pred match {
                case Some(node) => node
                case None => throw new Exception("FingerTable.pred -> Pas de pred")
                case null => throw new Exception("FingerTable.pred -> pred null")
            }
    }


    /**
     * Créer la
     * @param id
     * @param n
     */
    def initTable(id: Int, n: Node): Unit ={
        this.nodes = List[Node]()
        for (i <- 1 to sqrt(Other.maxPeers).toInt) {
            var nodeId: Int = (id + pow(2.0, i.toDouble).toInt) % Other.maxPeers
            this.nodes = this.nodes.:+(n.findPred(nodeId))
        }
//        println(this)
    }


    override def toString = s"FingerTable(nodes=$nodes, succ=$succ, pred=$pred, succID=$succID, predID=$predID)"
}