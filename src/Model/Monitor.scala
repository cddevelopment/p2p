package Model

import java.io.{OutputStream, IOException, InputStream}
import java.net.{Socket, ServerSocket}

import Network.Packet

import scala.collection.SortedMap

object Monitor {
    // ID, (IP, FingerTable)
    var nodes: SortedMap[Int, (String, FingerTable)] = SortedMap[Int, (String, FingerTable)]()
    // (IP, ID)
    var hashs: SortedMap[String, Int] = SortedMap[String, Int]()

    var hashsCollection: List[Int] = List.range(0, Other.maxPeers)

//    BEGIN hack : erreur de cast
var lastNumero: Int = 0
//    END hack : erreur de cast
}

class Monitor extends Runnable {

    val serverSocket = new ServerSocket(9999)

    override def run(): Unit = {
        println("Démarrage du serveur monitor")

        new Thread(new MonitorShellThread()).start()

        try {
            while(true) {
                val socket: Socket = serverSocket.accept()

                new Thread(new MonitorThread(socket)).start()
            }
        } catch {
            case ioe: IOException => println("Exception : " + ioe)
        }
    }

}

class MonitorThread(socket: Socket) extends Runnable {

    override def run(): Unit = {

        println("\nNouveau MonitorThread")

        val socketIP: String = socket.getInetAddress.getHostName

        val is: InputStream = socket.getInputStream
        val os: OutputStream = socket.getOutputStream

        while (!socket.isClosed) {

            val packet: Packet = Packet.read(is, socket.getInetAddress.getHostName)
            val header: String = packet.getHeader

            Log.log("Packet received : " + packet)

            packet.getHeader match {
                case Method.hash => {
                    val ip: String = packet.getData.asInstanceOf[String]
                    if (Monitor.nodes.map(node => node._2._1).exists(_ == ip)) {
                        //    BEGIN hack : erreur de cast
                      val hash: Int = Monitor.nodes.foldLeft()((acc, node) => if (node._2._1 == ip) node._1).asInstanceOf[Int]
                        Monitor.hashs = Monitor.hashs + (ip -> hash)
//                        val hash: Int = Monitor.lastNumero + 1
//                        Monitor.lastNumero += 1
                        //    END hack : erreur de cast

                        Packet.write(os, socketIP, new Packet(Method.hashResp, new Integer(hash)))
                    }else {
                        if (Monitor.hashsCollection.size > 0) {
                            val hash = Monitor.hashsCollection.head
                            Monitor.hashs = Monitor.hashs + (packet.getData.asInstanceOf[String] -> hash)
                            Monitor.hashsCollection = Monitor.hashsCollection.tail
                            Packet.write(os, socketIP, new Packet(Method.hashResp, new Integer(hash)))
                        }
                    }
                }



                case Method.ipFromId =>
                    Log.log("ipFromId request")
                    Log.log("hash : " + Monitor.hashs)
                    val packetID: Int = packet.getData.asInstanceOf[Int]
                    Monitor.hashs.find(t => t._2.equals(packetID)) match {
                        case Some(entry) =>
                            Log.log("Have found someting : " + entry + " ; rep sent")
                            Packet.write(os, socketIP, new Packet(Method.ipFromIdResp, Some(entry._1)))
                        case None =>
                            Log.log("Pas d'id correspondant")
                            Packet.write(os, socketIP, new Packet(Method.ipFromIdResp, None))


                    }




                case Method.connection => {
                    val data: Tuple3[String, Int, FingerTable] = packet.getData.asInstanceOf[Tuple3[String, Int, FingerTable]]

                    if (!Monitor.nodes.contains(data._2)) {
                        Monitor.nodes = Monitor.nodes + (data._2 -> (data._1, data._3))
                        println("Connexion d'un nouveau Node (" + data._2 + ")")
                    }
                }
                case Method.fingerTableUpdate => {
                    val data: Tuple2[Int, FingerTable] = packet.getData.asInstanceOf[Tuple2[Int, FingerTable]]
                    // Monitor.nodes.get(data._1).get
                    // TODO update ft in Monitor.nodes
                }
                case Method.message => println(packet.getData)
                case Method.file =>
                case Method.predOf => println("predOf")
                case Method.repPredOf => println("repPredOf")
                case _ => println("Header inconnu")
            }
        }
    }
}

class MonitorShellThread extends Runnable {

    override def run(): Unit = {

        while (true) {

            print("> ")

            var input: String = null
            while (input == null){
                val i = io.Source.stdin.getLines()
                if (i.nonEmpty) {
                    input = i.next.trim
                    input match {
                        case "nodes" =>
                            println("Nodes : " + Monitor.nodes.map(node => "IP: " + node._2._1 + ", ID : " + node._1))
                        case "hashs" =>
                            println("Hashs : " + Monitor.hashs.map(node => "IP: " + node._1 + ", ID : " + node._2))
                        case inp if inp.startsWith("fTable ") =>
                            val id: Int = input.split(" ")(1).toInt
                            if (Monitor.nodes.contains(id)) {
                                Monitor.nodes.get(id) match {
                                    case Some(data: Tuple2[Int, FingerTable]) => println(data._2)
                                    case None =>
                                }
                            }else {
                                println("L'identifiant n'existe pas")
                            }
                        case "help" =>
                            println("  - nodes : Affiche les pairs connectés")
                            println("  - fTable <id> : Affiche la table de routage du pair d'identifiant <id>")
                        case _ => println("Commande inconnue")
                    }
                }
            }
        }
    }
}