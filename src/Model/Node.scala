package Model

import Network.{Packet, PeerClient}

class Node (ip: String) {

    var id: Int = 0 // Hash.md5(this.ip)
    var fingerTable: FingerTable = null

    // Connexion au moniteur
    val peerMonitor: PeerClient = new PeerClient(Other.serverIP, Port.monitor)
    new Thread(peerMonitor).start()

    // Recupération d'un id (hash)
    peerMonitor.send(new Packet(Method.hash, ip))
    id = peerMonitor.read().getData.asInstanceOf[Int]

//    On créer la finger table
    this.install()
    println(this.fingerTable)

    // Connexion définitive (envoie de l'ip, l'id et de la fingerTable) au moniteur
    peerMonitor.send(new Packet(Method.connection, (ip, id, fingerTable)))


    var packetRecived: List[Packet] = List[Packet]()

    var waitingForPacket: Boolean = false

    def addPacketRecived(p: Packet): Unit ={
        packetRecived = packetRecived.:+(p)
    }


    /**
     * Permet d'installer un peer sur le réseau
     */
    def install(): Unit ={
        // --------------------------
        // ETAPE 1 : On se positionne
        // --------------------------

        this.fingerTable = new FingerTable(this)

        if (id - 1 > 0){
            idToIp(id - 1) match {
                case Some(ipSucc) =>
                    sendAndClose(ipSucc, Port.server, Method.setSucc, this)
                case None => Log.log("Erreur init fingertable")
            }
        }





        // On récupère le prédécéseur
//        Log.log("Req node pred id : " + this.id)
//        val nodePred: Node = this.findPred(this.id)
//        Log.log("Find node pred : " + nodePred)

        // On va tricher un peu considérer que l'on connait déjà l'id du node qui est notre prédecesseur
//        var idInput = ""
//        val i = io.Source.stdin.getLines()
//        if (i.hasNext) idInput = i.next()


//
//        // Si le précesseur n'est pas this
//        if (!nodePred.equals(this)){
//            Log.log("Je ne suis pas mon propre prédecesseur")
//            this.sendAndClose(nodePred.getIP(), Port.server, Method.setPred, this)
//            this.setPred(nodePred)
//            nodePred.myClosestSucc() match {
//                case Some(n: Node) =>
//                    this.sendAndClose(n.getIP(), Port.server, Method.setSucc, this)
//                    this.setSucc(n)
//                case None => println("FAIL : install - myClosestSucc -> None")
//            }
//            // ----------------------------------
//            // ETAPE 2 : On créer sa finger table
//            // ----------------------------------
//            this.fingerTable.initTable(id, this)
//            peerMonitor.send(new Packet(Method.fingerTableUpdate, (id, fingerTable)))
//        }else{
//            Log.log("Je suis mon propre pred")
//        }

    }





    /**
     * Recherche le successeur le plus proche du noeud désigné par l'id
     * @param id id du noeud en question
     * @return successeur le plus proche du noeud désigné par l'id
     */
    def	findSucc(id: Int): Unit ={
        this.findPred(id).myClosestSucc()
    }

    /**
     * Recherche le prédecesseur le plus proche du noeud désigné par l'id
     * @param id id du noeud en question
     * @return le prédecesseur le plus proche du noeud désigné par l'id
     */
    def findPred(id: Int): Node = {
        Log.log("BEGIN - findPred")
        val pred: Node = this.fingerTable.pred(id)
        Log.log("My pred is " + pred)
        this.myClosestPred() match {
            case Some(node) =>
                if (!node.equals(pred)){
                    Log.log("My pred != " + node)
                    this.reqPred(pred, id)
                } else {
                    Log.log("My pred == " + node)
                    pred
                }
            case None =>
                Log.log("myClosestPred is None")
                this.reqPred(pred, id)
        }
//        if (!pred.equals(this.myClosestPred()))
//            this.reqPred(pred, id)
//        else
//            pred
//        Log.log("END - findPred")
    }

    /**
     * Connexion au pair node pour récupérer le pred de l'id
     * @param node le pair a qui on fait la requete
     * @param id l'id dont on cherche le pred
     * @return
     */
    def reqPred(node: Node, id: Int): Node = {
        // On créer un client et un serveur spécifique pour cet échange

        val portServer = Port.server2
        val portClient = Port.monitor
//        val ps = new PeerServer2(portServer, this)
//        new Thread(ps).start()


        this.transmitReqPredOf(node.getIP(), (id, (this.ip, portServer)), portClient)

        val rep: Packet = this.waitForPacket()
        rep.getData.asInstanceOf[Node]
    }


    /**
     * Transmet la requete pour connaitre le predecesseur
     * Créer une connexion avec le client désigné par l'id et transmet le packet
     * @param ip le noeud que l'on veut contacter
     * @param data les données à transmettre
     * @param portClient le port à contacter
     * @return
     */
    def transmitReqPredOf(ip: String, data: Object, portClient: Int): Unit = {
        val pc = new PeerClient(ip, portClient)
        new Thread(pc).start()

        // TODO : Utiliser sendAndClose ?

        // On contacte le pair qui est le prédécesseur le plus proche de notre point de vue pour savoir si c'est bien le cas
        //        pc.send(new Packet(Method.predOf, (id, (this.ip, portServer))))
        val packet = new Packet(Method.predOf, data)
        Log.log("Packet envoyé : " + packet)
        pc.send(packet)
    }


    /**
     * On attend une réponse de l'interlocuteur
     * @return le packet reçu
     */
    def waitForPacket(): Packet = {
        var packet: Packet = null
        while(this.waitingForPacket){
            if (packetRecived.nonEmpty){
                this.waitingForPacket = false
                packet = packetRecived.head
            }
        }
        packet
    }


    /**
     * Envoie un packet contenant une methode et une donnée a un port sur un host et ferme la connexion
     * @param host l'ip du serveur
     * @param port le port du serveur
     * @param method la méthode
     * @param data la donnée à envoyer
     */
    def sendAndClose(host: String, port: Int, method: String, data: Object): Unit = {
        val pc = new PeerClient(host, port)
        new Thread(pc).start()



        pc.send(new Packet(method, data))

        pc.close()
    }

    /**
     * Si le noeud cherché est le suivant de this, on répond à l'expéditeur que this est le noeud précédent
     * Sinon, on transmet la requète au noeud prédécesseur le plus proche de l'idShearched a notre connaissance
     * Méthode appelée quand le serveur reçoit un packet avec une méthode PredOf
     * @param p le paquet à transmettre
     */
    def isPredOf(p: Packet): Unit = {
        val data = p.getData.asInstanceOf[(Int, (String, Int))]
        val (idShearched, (host, port)) = data
        this.fingerTable.succ match {
            case Some(node) =>
                if (node.getID() == idShearched){
                    // On peut répondre a l'expéditeur on disant qu'on l'a trouvé
                    this.sendAndClose(host, port, Method.repPredOf, this)
                }else{
                    // On doit continuer à chercher le pred
                    this.transmitReqPredOf(node.getIP(), (id, (this.ip, port)), 9991)

                }
            case None => println("Erreur - isPredOF")
        }
    }

    /**
     * Défini le predecesseur de this dans la fingerTable
     * @param n la node en question
     */
    def setPred(n: Node): Unit ={
        this.fingerTable.pred = Some(n)
    }


    /**
     * Défini le successeur de this dans la fingerTable
     * @param n la node en question
     */
    def setSucc(n: Node): Unit ={
        this.fingerTable.succ = Some(n)
        this.fingerTable.succID = n.id
    }


    def myClosestSucc(): Option[Node] ={
        this.fingerTable.succ
    }

    def myClosestPred(): Option[Node] ={
        this.fingerTable.pred
    }

    def init(): Unit ={

    }

    def notifyMonitor(): Unit ={

    }


    def maxId(node: Node): Node = {
        if (this.id > node.id) this
        else node
    }

    def setID(hash: Int): Unit = {
        println("hash")
        this.id = hash
    }

    def getIP(): String = ip
    def getID(): Int = id


    /**
     * Demande au monitor quel est l'ip qui correspond à un id
     * Méthode de hashage
     *
     * @param id l'id en question
     * @return l'ip
     */
    def idToIp(id: Int): Option[String] = {
        Log.log("Requète au miniteur pour savoir à qui correspond l'id " + id)
        val peerMonitor2: PeerClient = new PeerClient(Other.serverIP, Port.monitor)
        new Thread(peerMonitor2).start()
        peerMonitor2.send(new Packet(Method.ipFromId, id.asInstanceOf[Object]))
        Log.log("Attente de réponse...")
        val packetReceived = peerMonitor2.read()
        Log.log("Un packet est reçu : " + packetRecived)
        packetReceived.getData.asInstanceOf[Option[String]]
    }







    def canEqual(other: Any): Boolean = other.isInstanceOf[Node]

    override def equals(other: Any): Boolean = other match {
        case that: Node =>
            (that canEqual this) &&
                    id == that.id
        case _ => false
    }

    override def hashCode(): Int = {
        val state = Seq(id)
        state.map(_.hashCode()).foldLeft(0)((a, b) => 31 * a + b)
    }

    override def toString = s"Node(id=$id)"
}