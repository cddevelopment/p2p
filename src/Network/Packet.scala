package Network

import java.io._
import scala.collection.mutable

@SerialVersionUID(114L)
class Packet(headers: String, data: Object) extends Serializable {
    override def toString() = "[Headers : " + this.headers + " ; data : " + this.data + "]"

    def getData = this.data
    def getHeader = this.headers
}

/**
 * Objet chargé de lire et d'écrire dans les flux d'entrée et de sortie
 */
object Packet {

    var objectInputStreamMnemoisation: mutable.Map[String, ObjectInputStream] = mutable.Map()
    var objectOutputStreamMnemoisation: mutable.Map[String, ObjectOutputStream] = mutable.Map()

    /**
     * Lit un flux d'entrée et renvoie le packet
     * @param fis le flux d'entrée
     * @return le packet lu
     */
    def	read(fis: InputStream, ip: String): Packet = {
        println(objectInputStreamMnemoisation)
//        val ois: ObjectInputStream = this.objectInputStreamMnemoisation getOrElseUpdate (ip, new ObjectInputStream(fis))
        val ois: ObjectInputStream = new ObjectInputStream(fis)
        println(objectInputStreamMnemoisation)
        val pp: Packet = ois.readObject().asInstanceOf[Packet]
        pp

        //ois.close()
    }

    /**
     * Ecrit dans le flux de sortie le packet serialisé
     * @param fos le flux de sortie
     * @param p le packet à envoyer
     */
    def	write(fos: OutputStream, ip: String, p: Packet): Unit = {
        println(objectOutputStreamMnemoisation)
//        val oos: ObjectOutputStream = this.objectOutputStreamMnemoisation getOrElseUpdate (ip, new ObjectOutputStream(fos))
        val oos: ObjectOutputStream = new ObjectOutputStream(fos)
        println(objectOutputStreamMnemoisation)
        oos.writeObject(p)

        //oos.close()
    }

}