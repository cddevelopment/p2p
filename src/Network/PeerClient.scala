package Network

import java.io._
import java.net.Socket

import scala.collection.mutable


class PeerClient(host: String, port: Int) extends Runnable {


    /**
     * File de packets à envoyer au serveur
     */
    private val toSend: mutable.Queue[Packet] = new mutable.Queue[Packet]

    private val socket: Socket = new Socket(host, port)


    override def run(): Unit = {
        println("Démarrage client : " + host + " , port : " + port)
        val os: OutputStream = this.socket.getOutputStream

        //while(!this.socket.isClosed) {

            while (toSend.nonEmpty) {
                Packet.write(os, socket.getInetAddress.getHostName,toSend.dequeue())
            }

        //}
    }

    /**
     * Ajouter un packet à la pile d'envoi
     * @param packet le packet en question
     */
    def send(packet: Packet): Unit = {
        this.toSend.enqueue(packet)
    }

    /**
     * Attend et lit le flux d'entrée
     * @return retourne le packet lu
     */
    def read(): Packet = {
        val is: InputStream = this.socket.getInputStream
        Packet.read(is, socket.getInetAddress.getHostName)
    }

    /**
     * Ferme le socket
     */
    def close(): Unit = {
        println("Fermeture client : " + host)
        //this.socket.close()
    }
}
