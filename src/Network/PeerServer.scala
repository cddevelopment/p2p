package Network

import java.io._
import java.net.{ServerSocket, Socket}

import Model._

/**
 * Capable d'envoyer et de recevoir des Pakets
 */

class PeerServer(port: Int, portClient: Int) extends Runnable {
    val serverSocket = new ServerSocket(port)

    override def run(): Unit = {
        println("Démarrage server")
        println("Entrer pour créer le client")
        var input: String = null
        while (input == null)
            input = io.Source.stdin.getLines.toString()

        val pc = new PeerClient("localhost", portClient)
        new Thread(pc).start()
        while(true) {
            val socket: Socket = serverSocket.accept()
            if (socket != null)
                println("SERVER : Signal")
            /*val peerThread = new PeerServerThread(socket)
            new Thread(peerThread).start()*/
        }
    }
}

/*

class PeerServer2(port: Int, node: Node) extends Runnable {
    val serverSocket = new ServerSocket(port)

    override def run(): Unit = {
        println("Démarrage server")

        val socket: Socket = serverSocket.accept()
        val is: InputStream = socket.getInputStream

        while(!socket.isClosed) {
            val packet: Packet = Packet.read(is)
            println("-----------")
            packet.getHeader match {
                case Method.message => println(packet.getData)
                case Method.file =>
                case Method.predOf => node.isPredOf(packet)
                case Method.repPredOf => node.addPacketRecived(packet)
                case Method.setPred => node.setPred(packet.getData.asInstanceOf[Node])
                case Method.setSucc => node.setSucc(packet.getData.asInstanceOf[Node])
                case _ => println("Header inconnu")
            }
        }
    }
}
*/

class PeerServer2(port: Int, node: Node) extends Runnable {
    val serverSocket = new ServerSocket(port)

    override def run(): Unit = {
        println("Démarrage server")

        while(true){
            val socket: Socket = serverSocket.accept()
            if (socket != null){
                val pst = new PeerServerThread(socket, node)
                new Thread(pst).start()
            }
        }

    }
}



class PeerServerThread(socket: Socket, node: Node) extends Runnable {

    override def run(): Unit = {
        println("New server thread")
        val is: InputStream = socket.getInputStream
        val packet: Packet = Packet.read(is, socket.getInetAddress.getHostName)
        Log.log("Packet received : " + packet)
        println("-----------")
        packet.getHeader match {
            case Method.message => println(packet.getData)
            case Method.file =>
            case Method.predOf => node.isPredOf(packet)
            case Method.repPredOf => node.addPacketRecived(packet)
            case Method.setPred => node.setPred(packet.getData.asInstanceOf[Node])
            case Method.setSucc => node.setSucc(packet.getData.asInstanceOf[Node])
            case Method.ipFromIdResp => // TODO : Completer
            case _ => println("Header inconnu")
        }

    }
}