package Util


package object Hash {

	/**
	 * Hash une string en md5
	 * https://stevenwilliamalexander.wordpress.com/2012/06/11/scala-md5-hash-function-for-scala-console/
	 * @param string
	 * @return
	 */
	def md5(string: String): String = {
		java.security.MessageDigest.getInstance("MD5").digest(string.getBytes()).map(0xFF & _).map { "%02x".format(_) }.foldLeft(""){_ + _}
	}

	def main(args: Array[String]) {
		assert(md5("test").equals("098f6bcd4621d373cade4e832627b4f6"))
	}

}
