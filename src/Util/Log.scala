

package object Log {

    def log(message: String): Unit ={
        println("--> " + message)
    }

}